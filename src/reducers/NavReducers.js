import { NavigationAction } from 'react-navigation';

// Take Root from AppNavigator.js
import { Root } from '../AppNavigator';

const firtsActions = Root.router.getActionForPathAndParams('FormInput')
const INITIAL_NAV_STATE = Root.router.getStateForAction({
    firtsActions,
})

export default (state = INITIAL_NAV_STATE, action) => {
    let nextState;
    switch (action.type) {
        default:
            nextState = Root.router.getStateForAction(action, state)
    }

    return state || nextState;
}