import { combineReducers } from 'redux';
import NavReducers from './NavReducers'

export default combineReducers({
    nav: NavReducers
})