import React, { Component } from 'react';
import { Card, Form, Input, Button, Item, Label, Text } from 'native-base';
import { connect } from 'react-redux';

class FormInput extends Component {
    render() {
        return (
            <Card style={{ flex: 1 }}>
                <Form style={{ flex: 1 }}>
                    <Item floatingLabel>
                        <Label style={{ paddingTop: 15, color: 'black' }}> Username </Label>
                        <Input style={{ marginTop: 10 }}/>
                    </Item>

                    <Item floatingLabel>
                        <Label style={{ paddingTop: 15, color: 'black' }}> Password </Label>
                        <Input style={{ marginTop: 10 }}/>
                    </Item>

                    <Item floatingLabel>
                        <Label style={{ paddingTop: 15, color: 'black' }}> Kategori </Label>
                        <Input style={{ marginTop: 10 }}/>
                    </Item>

                    <Button block success style={{ marginTop: 15, }}>
                        <Text> Tambahkan </Text>
                    </Button>
                </Form>
            </Card>
        )
    }
}

export default FormInput;