import React, { Component } from 'react';
import { Button, Form, Text, Card } from 'native-base';

export class Main extends Component {
    render() {
        const { Container } = Style
        const { goBack } = this.props.navigation;
        return (
            <Form style={Container}>
                <Button block onPress={() => goBack('FormInput')} >
                    <Text> Isi Data </Text>
                </Button>

                <Button block style={{ marginTop: 5 }}>
                    <Text> Lihat Data </Text>
                </Button>
            </Form>
        )
    }
}

const Style = {
    Container: {
        flex: 1,
        justifyContent: 'center',

    }
}
