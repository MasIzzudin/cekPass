import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { StackNavigator, addNavigationHelpers } from 'react-navigation';

// Component
import { Main } from './screens/Main'
import FormInput from './screens/FormInput'

export const Root = StackNavigator({
    Main: {
        screen: Main,
        navigationOptions: { header: null }
    },
    FormInput: {
        screen: FormInput,
        navigationOptions: {
            title: 'Masukkan Data',
        }
    }
})

const AppNavigator = ({ dispatch, nav }) => {
    return (
        <Root navigation={addNavigationHelpers({ dispatch, state: nav })} />
    );
}

AppNavigator.propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    nav: state.nav
})

export default connect(mapStateToProps)(AppNavigator);