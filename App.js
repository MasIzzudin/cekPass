import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import Thunk from 'redux-thunk';
import reducers from './src/reducers';
import AppNavigator from './src/AppNavigator'

export class App extends Component {
  render() {
    return(
      <Provider store={createStore(reducers, applyMiddleware(Thunk))}>
        <AppNavigator/>
      </Provider>
    )
  }
}